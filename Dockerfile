FROM pytorch/pytorch:1.2-cuda10.0-cudnn7-runtime

## ensure locale is set during build
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN pip install dgl-cu100 && \
    pip install torchsummary && \
    pip install nodejs && \
    pip install tensorflow && \
    pip install keras && \
    pip install uproot && \
    pip install jupyter && \
    pip install jupyterlab && \
    pip install matplotlib && \
    pip install seaborn && \
    pip install hep_ml && \
    pip install sklearn && \
    pip install tables && \
    pip install dask[complete] && \
    pip install papermill pydot Pillow && \
    pip install iminuit && \
    pip install pyhf && \
    pip install energyflow imageio
    
RUN apt-get update && \
    apt-get install -y git debconf-utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user && \
    apt-get install -y vim less screen graphviz python3-tk wget && \
    apt-get install -y jq tree hdf5-tools bash-completion && \
    apt-get update && apt-get install -y git

RUN pip install git+https://github.com/rtqichen/torchdiffeq
    
## run jupyter notebook by default unless a command is specified
CMD ["jupyter", "notebook", "--port", "33333", "--no-browser"]
